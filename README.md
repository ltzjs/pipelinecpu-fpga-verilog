# Pipeline CPU based on FPGA and Verilog.（基于FPGA的流水线CPU）

#### 介绍
This is a Verilog implementation of pipeline CPU on FPGA board.

#### 软件架构
本项目为基于Verilog开发的运行于FPGA板上的流水线CPU，共支持38条指令。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

