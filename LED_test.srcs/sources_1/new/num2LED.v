`timescale 1ns / 1ps

module num2LED(
    input [3:0] num,
    output [7:0] LED
    );
    assign LED[0] = ~(num == 4'h1 || num == 4'h4 || num == 4'hb || num == 4'hc || num == 4'hd); // segment a
    assign LED[1] = ~(num == 4'h5 || num == 4'h6 || num == 4'hb || num == 4'hc || num == 4'he || num == 4'hf); // segment b
    assign LED[2] = ~(num == 4'h2 || num == 4'hc || num == 4'he || num == 4'hf); // segment c
    assign LED[3] = ~(num == 4'h1 || num == 4'h4 || num == 4'h7 || num == 4'ha || num == 4'hf); // segment d
    assign LED[4] = ~(num == 4'h1 || num == 4'h3 || num == 4'h4 || num == 4'h5 || num == 4'h7 || num == 4'h9); // segment e
    assign LED[5] = ~(num == 4'h1 || num == 4'h2 || num == 4'h3 || num == 4'hc || num == 4'hd); // segment f
    assign LED[6] = ~(num == 4'h0 || num == 4'h1 || num == 4'h7); // segment g
    assign LED[7] = 0; // dp

endmodule
