.text

    j main_start
    nop

delay_2s:
    lui $a0,0x02fa
    ori $a0,0xf080
delay_2s_lbl:
    addiu $a0,$a0,-1
    nop
    bne $a0,$zero,delay_2s_lbl
    nop
    jr $ra
    nop

delay_1s:
    lui $a0,0x017d
    ori $a0,0x7840
delay_1s_lbl:
    addiu $a0,$a0,-1
    nop
    bne $a0,$zero,delay_1s_lbl
    nop
    jr $ra
    nop

delay_0_5s:
    lui $a0,0x00be
    ori $a0,0xbc20
delay_0_5s_lbl:
    addiu $a0,$a0,-1
    nop
    bne $a0,$zero,delay_0_5s_lbl
    nop
    jr $ra
    nop

delay_0_25s:
    lui $a0,0x005f
    ori $a0,0x5e10
delay_0_25s_lbl:
    addiu $a0,$a0,-1
    nop
    bne $a0,$zero,delay_0_25s_lbl
    nop
    jr $ra
    nop

delay_0_125s:
    lui $a0,0x002f
    ori $a0,0xaf08
delay_0_125s_lbl:
    addiu $a0,$a0,-1
    nop
    bne $a0,$zero,delay_0_125s_lbl
    nop
    jr $ra
    nop

main_start:

    addiu $t1,$zero,-1 # banner
    sw $t1,0
    jal delay_2s
    nop

    lui $t1,0x1122
    ori $t1,0x3344
    sw $t1,0
    jal delay_1s
    nop
    lui $t2,0x5566
    ori $t2,0x7788
    sw $t2,0
    jal delay_1s
    nop
    addu $t3,$t1,$t2
    sw $t3,0
    jal delay_2s
    nop
    sw $t1,0
    jal delay_1s
    nop
    addi $t1 $t1,1
    sw $t1,0
    jal delay_0_5s
    nop
    addi $t1 $t1,1
    sw $t1,0
    jal delay_0_5s
    nop
    addiu $t1 $t1,1
    sw $t1,0
    jal delay_0_5s
    nop
    addiu $t1 $t1,1
    sw $t1,0
    jal delay_2s
    nop

    addiu $t1,$zero,-1 # banner
    sw $t1,0
    jal delay_2s
    nop

    addiu $t1,$zero,-2
    sw $t1,0
    jal delay_1s
    nop
    addiu $t2,$zero,1
    sw $t2,0
    jal delay_1s
    nop

    lui $t4,0x8888
    ori $t4,0x8888 # delimeter
    sw $t4,0
    jal delay_0_5s
    nop

    slti $t3,$t1,0 # 1
    sw $t3,0
    jal delay_1s
    nop

    lui $t4,0x8888
    ori $t4,0x8888 # delimeter
    sw $t4,0
    jal delay_0_5s
    nop

    sltiu $t3,$t1,0 # 0
    sw $t3,0
    jal delay_1s
    nop

    lui $t4,0x8888
    ori $t4,0x8888 # delimeter
    sw $t4,0
    jal delay_0_5s
    nop

    slti $t3,$t2,0 # 0
    sw $t3,0
    jal delay_1s
    nop

    lui $t4,0x8888
    ori $t4,0x8888 # delimeter
    sw $t4,0
    jal delay_0_5s
    nop

    sltiu $t3,$t2,0 # 0
    sw $t3,0
    jal delay_1s
    nop

    lui $t4,0x8888
    ori $t4,0x8888 # delimeter
    sw $t4,0
    jal delay_0_5s
    nop

    slt $t3,$t1,$t2 # 1
    sw $t3,0
    jal delay_1s
    nop

    lui $t4,0x8888
    ori $t4,0x8888 # delimeter
    sw $t4,0
    jal delay_0_5s
    nop

    slt $t3,$t2,$t1 # 0
    sw $t3,0
    jal delay_1s
    nop

    lui $t4,0x8888
    ori $t4,0x8888 # delimeter
    sw $t4,0
    jal delay_0_5s
    nop

    addiu $t1,$zero,-1 # banner
    sw $t1,0
    jal delay_2s
    nop

    lui $t1,0xabcd
    ori $t1,0xef01
    sw $t1,0
    jal delay_2s
    nop

    srl $t1,$t1,4
    sw $t1,0
    jal delay_0_5s
    nop
    srl $t1,$t1,4
    sw $t1,0
    jal delay_0_5s
    nop
    srl $t1,$t1,4
    sw $t1,0
    jal delay_0_5s
    nop
    srl $t1,$t1,4
    sw $t1,0
    jal delay_0_5s
    nop
    srl $t1,$t1,4
    sw $t1,0
    jal delay_0_5s
    nop
    srl $t1,$t1,4
    sw $t1,0
    jal delay_2s
    nop

    sll $t1,$t1,4
    sw $t1,0
    jal delay_0_5s
    nop
    sll $t1,$t1,4
    sw $t1,0
    jal delay_0_5s
    nop
    sll $t1,$t1,4
    sw $t1,0
    jal delay_0_5s
    nop
    sll $t1,$t1,4
    sw $t1,0
    jal delay_0_5s
    nop
    sll $t1,$t1,4
    sw $t1,0
    jal delay_0_5s
    nop
    sll $t1,$t1,4
    sw $t1,0
    jal delay_2s
    nop

    sra $t1,$t1,4
    sw $t1,0
    jal delay_0_5s
    nop
    sra $t1,$t1,4
    sw $t1,0
    jal delay_0_5s
    nop
    sra $t1,$t1,4
    sw $t1,0
    jal delay_0_5s
    nop
    sra $t1,$t1,4
    sw $t1,0
    jal delay_0_5s
    nop
    sra $t1,$t1,4
    sw $t1,0
    jal delay_0_5s
    nop
    sra $t1,$t1,4
    sw $t1,0
    jal delay_0_5s
    nop
    sra $t1,$t1,4
    sw $t1,0
    jal delay_2s
    nop

    addiu $t1,$zero,-1 # banner
    sw $t1,0
    jal delay_2s
    nop

    lui $t1,0x3333
    ori $t1,0x6666
    sw $t1,0
    jal delay_1s
    nop
    lui $t2,0x6666
    ori $t2,0x3333
    sw $t2,0
    jal delay_1s
    nop

    and $t3,$t1,$t2 # 0x22222222
    sw $t3,0
    jal delay_2s
    nop
    or $t3,$t1,$t2 # 0x77777777
    sw $t3,0
    jal delay_2s
    nop
    xor $t3,$t1,$t2 # 0x55555555
    sw $t3,0
    jal delay_2s
    nop
    nor $t3,$t1,$t2 # 0x88888888
    sw $t3,0
    jal delay_2s
    nop

    lui $t1,0xcccc # end
    ori $t1,0xcccc # end
    sw $t1,0
    jal delay_2s
    nop

    addiu $t1,$zero,0
    sw $t1,0
    nop
