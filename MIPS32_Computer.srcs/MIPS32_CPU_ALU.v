`timescale 1ns / 1ps
`include "MIPS32_CPU_ALU_OPTR.v"

module MIPS32_CPU_ALU(
    input [31:0] optr,
    input [31:0] opnd_a,
    input [31:0] opnd_b,
    output [31:0] res
    );

    wire signed [31:0] opnd_a_signed = opnd_a;
    wire signed [31:0] opnd_b_signed = opnd_b;
    wire [4:0] inst_sa = optr[2] ? opnd_a[4:0] : opnd_a[10:6];

    assign res = optr == `OPTR_ADD ? opnd_a + opnd_b
    : optr == `OPTR_ADDU ? opnd_a + opnd_b
    : optr == `OPTR_SLT ? opnd_a_signed < opnd_b_signed ? 32'b1 : 32'b0
    : optr == `OPTR_SLTU ? opnd_a < opnd_b ? 32'b1 : 32'b0
    : optr == `OPTR_SUB ? opnd_a - opnd_b
    : optr == `OPTR_SUBU ? opnd_a - opnd_b
    : optr == `OPTR_AND ? opnd_a & opnd_b
    : optr == `OPTR_ANDI ? opnd_a & {16'b0, opnd_b[15:0]}
    : optr == `OPTR_OR ? opnd_a | opnd_b
    : optr == `OPTR_ORI ? opnd_a | {16'b0, opnd_b[15:0]}
    : optr == `OPTR_XOR ? opnd_a ^ opnd_b
    : optr == `OPTR_XORI ? opnd_a ^ {16'b0, opnd_b[15:0]}
    : optr == `OPTR_NOR ? ~(opnd_a | opnd_b)
    : optr == `OPTR_LUI ? {opnd_b[15:0], 16'b0}
    : optr == `OPTR_SLL || optr == `OPTR_SLLV ? opnd_b << inst_sa
    : optr == `OPTR_SRL || optr == `OPTR_SRLV ? opnd_b >> inst_sa
    : optr == `OPTR_SRA || optr == `OPTR_SRAV ? {{32{opnd_b[31]}}, opnd_b} >>> inst_sa
    : 32'b0;

endmodule
