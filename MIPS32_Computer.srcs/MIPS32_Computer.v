`timescale 1ns / 1ps

module MIPS32_Computer(
    input clk,
    input rst_n,
    output [7:0] LED_0,
    output [7:0] LED_1,
    output [7:0] dn
    );

    wire [31:0] inst_data;
    wire [31:0] data_in;
    wire inst_en;
    wire [31:0] inst_addr;
    wire data_we;
    wire [31:0] data_addr;
    wire [31:0] data_out;

    reg [31:0] data_out_REG;

    always @(posedge clk or negedge rst_n) begin
        if (!rst_n) begin
            data_out_REG <= 32'b0;
        end else if (data_we) begin
            data_out_REG <= data_out;
        end
    end

    wire [63:0] LED_out;

    num2LED num2LED_0(
        .num(data_out_REG[3:0]),
        .LED(LED_out[7:0])
    );
    num2LED num2LED_1(
        .num(data_out_REG[7:4]),
        .LED(LED_out[15:8])
    );
    num2LED num2LED_2(
        .num(data_out_REG[11:8]),
        .LED(LED_out[23:16])
    );
    num2LED num2LED_3(
        .num(data_out_REG[15:12]),
        .LED(LED_out[31:24])
    );
    num2LED num2LED_4(
        .num(data_out_REG[19:16]),
        .LED(LED_out[39:32])
    );
    num2LED num2LED_5(
        .num(data_out_REG[23:20]),
        .LED(LED_out[47:40])
    );
    num2LED num2LED_6(
        .num(data_out_REG[27:24]),
        .LED(LED_out[55:48])
    );
    num2LED num2LED_7(
        .num(data_out_REG[31:28]),
        .LED(LED_out[63:56])
    );

    reg [7:0] LED_0_REG;
    reg [7:0] LED_1_REG;
    reg [3:0] dn_REG;
    reg [31:0] clk_counter;

    assign LED_0 = LED_0_REG;
    assign LED_1 = LED_1_REG;
    assign dn[3:0] = dn_REG;
    assign dn[7:4] = dn_REG;

    wire [3:0] dn_REG_next = dn_REG[3] ? 4'b0001
                           : dn_REG[0] ? 4'b0010
                           : dn_REG[1] ? 4'b0100
                           : 4'b1000;

    always @(posedge clk or negedge rst_n) begin
        if (!rst_n) begin
            LED_0_REG <= 8'hff;
            LED_1_REG <= 8'hff;
            dn_REG <= 4'hf;
            clk_counter <= 32'b0;
        end else if (clk_counter[19]) begin
            LED_0_REG <= ({8{dn_REG_next[0]}} & LED_out[7:0])
                       | ({8{dn_REG_next[1]}} & LED_out[15:8])
                       | ({8{dn_REG_next[2]}} & LED_out[23:16])
                       | ({8{dn_REG_next[3]}} & LED_out[31:24]);
            LED_1_REG <= ({8{dn_REG_next[0]}} & LED_out[39:32])
                       | ({8{dn_REG_next[1]}} & LED_out[47:40])
                       | ({8{dn_REG_next[2]}} & LED_out[55:48])
                       | ({8{dn_REG_next[3]}} & LED_out[63:56]);
            dn_REG <= dn_REG_next;
            clk_counter <= 32'b0;
        end else begin
            clk_counter <= clk_counter + 32'b1;
        end
    end

    MIPS32_CPU CPU (
        // input:
        .clk(clk),
        .rst_n(rst_n),
        .inst_data(inst_data),
        .data_in(data_in),
        // output:
        .inst_en(inst_en),
        .inst_addr(inst_addr),
        .data_we(data_we),
        .data_addr(data_addr),
        .data_out(data_out)
    );

    ROM32 ROM32_inst (
        .clka(clk),    // input wire clka
        .ena(inst_en),      // input wire ena
        .addra(inst_addr[11:2]),  // input wire [9 : 0] addra
        .douta(inst_data)  // output wire [31 : 0] douta
    );

    RAM32 RAM32_data (
        .clka(clk),    // input wire clka
        .wea(data_we),      // input wire [0 : 0] wea
        .addra(data_addr[9:0]),  // input wire [9 : 0] addra
        .dina(data_out),    // input wire [31 : 0] dina
        .clkb(clk),    // input wire clkb
        .addrb(data_addr[9:0]),  // input wire [9 : 0] addrb
        .doutb(data_in)  // output wire [31 : 0] doutb
    );

endmodule
