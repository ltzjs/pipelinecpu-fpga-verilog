`timescale 1ns / 1ps

module MIPS32_CPU_REG_IF_ID(
    input clk,
    input rst_n,
    input pause,
    input [31:0] pc_p4_IF,
    output [31:0] pc_p4_ID
    );

    reg [31:0] pc_p4_REG;

    assign pc_p4_ID = pc_p4_REG;

    always @(posedge clk or negedge rst_n) begin
        if (!rst_n) begin
            pc_p4_REG <= 32'b0;
        end else if (!pause) begin
            pc_p4_REG <= pc_p4_IF;
        end
    end

endmodule
