`timescale 1ns / 1ps
`include "MIPS32_CPU_OPCODE.v"

module MIPS32_CPU_Branch_Jump_Control_Unit(
    input [5:0] inst_opcode,
    input [4:0] inst_rt,
    input [5:0] inst_function,
    input [31:0] pc_p4,
    // for branch:
    input [31:0] register_file_read_data_a_updated,
    input [31:0] register_file_read_data_b_updated,
    input [31:0] sign_extended_inst_immediate,
    // for jump:
    input [25:0] inst_instr_index,

    output pc_next_is_pc_p4,
    output [31:0] pc_branch_jump,

    output register_file_we_Branch_Jump_Control_Unit,
    output register_file_write_addr_is_31,
    output [31:0] register_file_write_data
    );

    wire [4:0] inst_opcode_hi = inst_opcode[5:1];
    wire [3:0] inst_rt_hi = inst_rt[4:1];
    wire [4:0] inst_function_hi = inst_function[5:1];

    wire rs_eq_rt = register_file_read_data_a_updated == register_file_read_data_b_updated;
    wire rs_lt_zero = register_file_read_data_a_updated[31];
    wire rs_le_zero = rs_lt_zero || register_file_read_data_a_updated == 32'b0;

    wire inst_is_J_JAL = inst_opcode_hi == 5'b000_01;
    wire inst_is_JR_JALR = inst_opcode == `OPCODE_SPECIAL && (inst_function_hi == 5'b001_00);
    wire will_branch = (inst_opcode_hi == 5'b000_10 && (inst_opcode[0] ^ rs_eq_rt)) // BEQ BNE
                    || (inst_opcode_hi == 5'b000_11 && (inst_opcode[0] ^ rs_le_zero)) // BLEZ BGTZ
                    || (inst_opcode == `OPCODE_REGIMM && inst_rt[3:1] == 3'b0 && (inst_rt[0] ^ rs_lt_zero)); // BLTZ BGEZ BLTZAL BGEZAL

    assign pc_next_is_pc_p4 = inst_is_J_JAL || inst_is_JR_JALR || will_branch ? 0 : 1;

    assign pc_branch_jump = inst_is_J_JAL ? {pc_p4[31:28], inst_instr_index, 2'b00}
                          : inst_is_JR_JALR ? register_file_read_data_a_updated
                          : pc_p4 + {sign_extended_inst_immediate[29:0], 2'b00}; // BEQ BNE BLEZ BGTZ BLTZ BGEZ BLTZAL BGEZAL

    assign register_file_we_Branch_Jump_Control_Unit = inst_opcode == `OPCODE_JAL || (inst_opcode == `OPCODE_SPECIAL && inst_function == `FUNCTION_JALR) // JAL JALR
                                                    || (inst_opcode == `OPCODE_REGIMM && inst_rt_hi == 4'b1000); // BLTZAL BGEZAL

    assign register_file_write_addr_is_31 = inst_opcode == `OPCODE_JAL || (inst_opcode == `OPCODE_REGIMM && inst_rt_hi == 4'b1000); // JAL BLTZAL BGEZAL
    assign register_file_write_data = pc_p4 + 32'd4;

endmodule
