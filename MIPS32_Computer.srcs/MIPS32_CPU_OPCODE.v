`define OPCODE_SPECIAL 6'o00
`define OPCODE_REGIMM 6'o01
`define OPCODE_J 6'o02
`define OPCODE_JAL 6'o03
`define OPCODE_BEQ 6'o04
`define OPCODE_BNE 6'o05
`define OPCODE_BLEZ 6'o06
`define OPCODE_BGTZ 6'o07

`define OPCODE_ADDI 6'o10
`define OPCODE_ADDIU 6'o11
`define OPCODE_SLTI 6'o12
`define OPCODE_SLTIU 6'o13
`define OPCODE_ANDI 6'o14
`define OPCODE_ORI 6'o15
`define OPCODE_XORI 6'o16
`define OPCODE_LUI 6'o17

// `define OPCODE_LB 6'o40
// `define OPCODE_LH 6'o41
// `define OPCODE_LWL 6'o42
`define OPCODE_LW 6'o43
// `define OPCODE_LBU 6'o44
// `define OPCODE_LHU 6'o45
// `define OPCODE_LWR 6'o46

// `define OPCODE_SB 6'o50
// `define OPCODE_SH 6'o51
// `define OPCODE_SWL 6'o52
`define OPCODE_SW 6'o53
// `define OPCODE_SWR 6'o56

// OPCODE_SPECIAL:
`define FUNCTION_SLL 6'o00
`define FUNCTION_SRL 6'o02
`define FUNCTION_SRA 6'o03
`define FUNCTION_SLLV 6'o04
`define FUNCTION_SRLV 6'o06
`define FUNCTION_SRAV 6'o07

`define FUNCTION_JR 6'o10
`define FUNCTION_JALR 6'o11

`define FUNCTION_ADD 6'o40
`define FUNCTION_ADDU 6'o41
`define FUNCTION_SUB 6'o42
`define FUNCTION_SUBU 6'o43
`define FUNCTION_AND 6'o44
`define FUNCTION_OR 6'o45
`define FUNCTION_XOR 6'o46
`define FUNCTION_NOR 6'o47

`define FUNCTION_SLT 6'o52
`define FUNCTION_SLTU 6'o53

// OPCODE_REGIMM:
`define RT_BLTZ 5'b00_000
`define RT_BGEZ 5'b00_001

`define RT_BLTZAL 5'b10_000
`define RT_BGEZAL 5'b10_001
