`timescale 1ns / 1ps
`include "MIPS32_CPU_OPCODE.v"
`include "MIPS32_CPU_ALU_OPTR.v"

module MIPS32_CPU_Control_Unit(
    input [5:0] inst_opcode,
    input [5:0] inst_function,
    output [31:0] alu_optr,
    output register_file_we_Control_Unit,
    output data_we,
    output alu_opnd_a_is_imm,
    output alu_opnd_b_is_imm,
    output register_file_write_addr_is_rt,
    output register_file_write_data_is_from_mem
    );

    wire [2:0] inst_opcode_hi = inst_opcode[5:3];
    wire [2:0] inst_function_hi = inst_function[5:3];

    assign alu_optr = inst_opcode == `OPCODE_SPECIAL ?
        inst_function == `FUNCTION_ADD ? `OPTR_ADD
        : inst_function == `FUNCTION_ADDU ? `OPTR_ADDU
        : inst_function == `FUNCTION_SUB ? `OPTR_SUB
        : inst_function == `FUNCTION_SUBU ? `OPTR_SUBU
        : inst_function == `FUNCTION_AND ? `OPTR_AND
        : inst_function == `FUNCTION_OR ? `OPTR_OR
        : inst_function == `FUNCTION_XOR ? `OPTR_XOR
        : inst_function == `FUNCTION_NOR ? `OPTR_NOR
        : inst_function == `FUNCTION_SLL ? `OPTR_SLL
        : inst_function == `FUNCTION_SRL ? `OPTR_SRL
        : inst_function == `FUNCTION_SRA ? `OPTR_SRA
        : inst_function == `FUNCTION_SLLV ? `OPTR_SLLV
        : inst_function == `FUNCTION_SRLV ? `OPTR_SRLV
        : inst_function == `FUNCTION_SRAV ? `OPTR_SRAV
        : inst_function == `FUNCTION_SLT ? `OPTR_SLT
        : inst_function == `FUNCTION_SLTU ? `OPTR_SLTU
        : `OPTR_NOP
    : inst_opcode == `OPCODE_ADDI ? `OPTR_ADD
    : inst_opcode == `OPCODE_ADDIU || inst_opcode == `OPCODE_LW || inst_opcode == `OPCODE_SW ? `OPTR_ADDU
    : inst_opcode == `OPCODE_SLTI ? `OPTR_SLT
    : inst_opcode == `OPCODE_SLTIU ? `OPTR_SLTU
    : inst_opcode == `OPCODE_ANDI ? `OPTR_ANDI
    : inst_opcode == `OPCODE_ORI ? `OPTR_ORI
    : inst_opcode == `OPCODE_XORI ? `OPTR_XORI
    : inst_opcode == `OPCODE_LUI ? `OPTR_LUI
    : `OPTR_NOP;

    assign register_file_we_Control_Unit = inst_opcode == `OPCODE_SPECIAL
                                        && (inst_function_hi == 3'o0 // SLL SRL SRA SLLV SRLV SRAV
                                            || inst_function[5:4] == 2'b10) // ADD ADDU SUB SUBU AND OR XOR NOR SLT SLTU
                                        || inst_opcode_hi == 3'o1 // ADDI ADDIU SLTI SLTIU ANDI ORI XORI LUI
                                        || inst_opcode_hi == 3'o4; // LB LH LWL LW LBU LHU LWR

    assign data_we = inst_opcode_hi == 3'o5; // SB SH SWL SW SWR CACHE

    assign alu_opnd_a_is_imm = inst_opcode == `OPCODE_SPECIAL && inst_function[5:2] == 4'b0; // SLL SRL SRA

    assign alu_opnd_b_is_imm = inst_opcode_hi == 3'o1 // ADDI ADDIU SLTI SLTIU ANDI ORI XORI LUI
                            || inst_opcode[5:4] == 2'b10; // LB LH LWL LW LBU LHU LWR SB SH SWL SW SWR CACHE

    assign register_file_write_addr_is_rt = inst_opcode_hi == 3'o1 // ADDI ADDIU SLTI SLTIU ANDI ORI XORI LUI
                                         || inst_opcode_hi == 3'o4; // LB LH LWL LW LBU LHU LWR

    assign register_file_write_data_is_from_mem = inst_opcode_hi == 3'o4; // LB LH LWL LW LBU LHU LWR

endmodule
