`timescale 1ns / 1ps

module MIPS32_CPU_REG_MEM_WB(
    input clk,
    input rst_n,

    input register_file_we_MEM,
    input [4:0] register_file_write_addr_MEM,
    input register_file_write_data_is_from_mem_MEM,
    input register_file_write_data_is_from_ID_MEM,
    input [31:0] register_file_write_data_ID_MEM,
    input [31:0] alu_res_MEM,

    output register_file_we_WB,
    output [4:0] register_file_write_addr_WB,
    output register_file_write_data_is_from_mem_WB,
    output register_file_write_data_is_from_ID_WB,
    output [31:0] register_file_write_data_ID_WB,
    output [31:0] alu_res_WB
    );

    reg register_file_we_REG;
    reg [4:0] register_file_write_addr_REG;
    reg register_file_write_data_is_from_mem_REG;
    reg register_file_write_data_is_from_ID_REG;
    reg [31:0] register_file_write_data_ID_REG;
    reg [31:0] alu_res_REG;

    assign register_file_we_WB = register_file_we_REG;
    assign register_file_write_addr_WB = register_file_write_addr_REG;
    assign register_file_write_data_is_from_mem_WB = register_file_write_data_is_from_mem_REG;
    assign register_file_write_data_is_from_ID_WB = register_file_write_data_is_from_ID_REG;
    assign register_file_write_data_ID_WB = register_file_write_data_ID_REG;
    assign alu_res_WB = alu_res_REG;

    always @(posedge clk or negedge rst_n) begin
        if (!rst_n) begin
            register_file_we_REG <= 0;
            register_file_write_addr_REG <= 5'b0;
            register_file_write_data_is_from_mem_REG <= 0;
            register_file_write_data_is_from_ID_REG <= 0;
            register_file_write_data_ID_REG <= 32'b0;
            alu_res_REG <= 32'b0;
        end else begin
            register_file_we_REG <= register_file_we_MEM;
            register_file_write_addr_REG <= register_file_write_addr_MEM;
            register_file_write_data_is_from_mem_REG <= register_file_write_data_is_from_mem_MEM;
            register_file_write_data_is_from_ID_REG <= register_file_write_data_is_from_ID_MEM;
            register_file_write_data_ID_REG <= register_file_write_data_ID_MEM;
            alu_res_REG <= alu_res_MEM;
        end
    end

endmodule
