`timescale 1ns / 1ps

module MIPS32_CPU_REG_ID_EX(
    input clk,
    input rst_n,

    input nop_ID_EX,

    input [31:0] sign_extended_inst_immediate_ID,

    input [31:0] alu_optr_ID,
    input register_file_we_ID,
    input data_we_ID,
    input alu_opnd_a_is_imm_ID,
    input alu_opnd_b_is_imm_ID,
    input [4:0] register_file_write_addr_ID,
    input register_file_write_data_is_from_mem_ID,
    input register_file_write_data_is_from_ID_ID,
    input [31:0] register_file_write_data_ID_ID,

    input [31:0] register_file_read_data_a_ID,
    input [31:0] register_file_read_data_b_ID,

    output [31:0] sign_extended_inst_immediate_EX,

    output [31:0] alu_optr_EX,
    output register_file_we_EX,
    output data_we_EX,
    output alu_opnd_a_is_imm_EX,
    output alu_opnd_b_is_imm_EX,
    output [4:0] register_file_write_addr_EX,
    output register_file_write_data_is_from_mem_EX,
    output register_file_write_data_is_from_ID_EX,
    output [31:0] register_file_write_data_ID_EX,

    output [31:0] register_file_read_data_a_EX,
    output [31:0] register_file_read_data_b_EX
    );

    reg [31:0] sign_extended_inst_immediate_REG;

    reg [31:0] alu_optr_REG;
    reg register_file_we_REG;
    reg data_we_REG;
    reg alu_opnd_a_is_imm_REG;
    reg alu_opnd_b_is_imm_REG;
    reg [4:0] register_file_write_addr_REG;
    reg register_file_write_data_is_from_mem_REG;
    reg register_file_write_data_is_from_ID_REG;
    reg [31:0] register_file_write_data_ID_REG;

    reg [31:0] register_file_read_data_a_REG;
    reg [31:0] register_file_read_data_b_REG;

    assign sign_extended_inst_immediate_EX = sign_extended_inst_immediate_REG;

    assign alu_optr_EX = alu_optr_REG;
    assign register_file_we_EX = register_file_we_REG;
    assign data_we_EX = data_we_REG;
    assign alu_opnd_a_is_imm_EX = alu_opnd_a_is_imm_REG;
    assign alu_opnd_b_is_imm_EX = alu_opnd_b_is_imm_REG;
    assign register_file_write_addr_EX = register_file_write_addr_REG;
    assign register_file_write_data_is_from_mem_EX = register_file_write_data_is_from_mem_REG;
    assign register_file_write_data_is_from_ID_EX = register_file_write_data_is_from_ID_REG;
    assign register_file_write_data_ID_EX = register_file_write_data_ID_REG;

    assign register_file_read_data_a_EX = register_file_read_data_a_REG;
    assign register_file_read_data_b_EX = register_file_read_data_b_REG;

    always @(posedge clk or negedge rst_n) begin
        if (!rst_n || nop_ID_EX) begin
            sign_extended_inst_immediate_REG <= 32'b0;

            alu_optr_REG <= 32'b0;
            register_file_we_REG <= 0;
            data_we_REG <= 0;
            alu_opnd_a_is_imm_REG <= 0;
            alu_opnd_b_is_imm_REG <= 0;
            register_file_write_addr_REG <= 5'b0;
            register_file_write_data_is_from_mem_REG <= 0;
            register_file_write_data_is_from_ID_REG <= 0;
            register_file_write_data_ID_REG <= 32'b0;

            register_file_read_data_a_REG <= 32'b0;
            register_file_read_data_b_REG <= 32'b0;
        end else begin
            sign_extended_inst_immediate_REG <= sign_extended_inst_immediate_ID;

            alu_optr_REG <= alu_optr_ID;
            register_file_we_REG <= register_file_we_ID;
            data_we_REG <= data_we_ID;
            alu_opnd_a_is_imm_REG <= alu_opnd_a_is_imm_ID;
            alu_opnd_b_is_imm_REG <= alu_opnd_b_is_imm_ID;
            register_file_write_addr_REG <= register_file_write_addr_ID;
            register_file_write_data_is_from_mem_REG <= register_file_write_data_is_from_mem_ID;
            register_file_write_data_is_from_ID_REG <= register_file_write_data_is_from_ID_ID;
            register_file_write_data_ID_REG <= register_file_write_data_ID_ID;

            register_file_read_data_a_REG <= register_file_read_data_a_ID;
            register_file_read_data_b_REG <= register_file_read_data_b_ID;
        end
    end

endmodule
