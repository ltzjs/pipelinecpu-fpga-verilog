`timescale 1ns / 1ps

module MIPS32_CPU_PC(
    input clk,
    input rst_n,
    input pause,
    input [31:0] pc_next,
    output [31:0] pc_IF
    );

    reg [31:0] pc_REG;

    assign pc_IF = pc_REG;

    always @(posedge clk or negedge rst_n) begin
        if (!rst_n) begin
            pc_REG <= 32'b0;
        end else if (!pause) begin
            pc_REG <= pc_next;
        end
    end

endmodule
