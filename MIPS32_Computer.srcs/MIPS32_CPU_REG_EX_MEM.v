`timescale 1ns / 1ps

module MIPS32_CPU_REG_EX_MEM(
    input clk,
    input rst_n,

    input register_file_we_EX,
    input data_we_EX,
    input [4:0] register_file_write_addr_EX,
    input register_file_write_data_is_from_mem_EX,
    input register_file_write_data_is_from_ID_EX,
    input [31:0] register_file_write_data_ID_EX,
    input [31:0] alu_res_EX,
    input [31:0] register_file_read_data_b_EX,

    output register_file_we_MEM,
    output data_we_MEM,
    output [4:0] register_file_write_addr_MEM,
    output register_file_write_data_is_from_mem_MEM,
    output register_file_write_data_is_from_ID_MEM,
    output [31:0] register_file_write_data_ID_MEM,
    output [31:0] alu_res_MEM,
    output [31:0] register_file_read_data_b_MEM
    );

    reg register_file_we_REG;
    reg data_we_REG;
    reg [4:0] register_file_write_addr_REG;
    reg register_file_write_data_is_from_mem_REG;
    reg register_file_write_data_is_from_ID_REG;
    reg [31:0] register_file_write_data_ID_REG;
    reg [31:0] alu_res_REG;
    reg [31:0] register_file_read_data_b_REG;

    assign register_file_we_MEM = register_file_we_REG;
    assign data_we_MEM = data_we_REG;
    assign register_file_write_addr_MEM = register_file_write_addr_REG;
    assign register_file_write_data_is_from_mem_MEM = register_file_write_data_is_from_mem_REG;
    assign register_file_write_data_is_from_ID_MEM = register_file_write_data_is_from_ID_REG;
    assign register_file_write_data_ID_MEM = register_file_write_data_ID_REG;
    assign alu_res_MEM = alu_res_REG;
    assign register_file_read_data_b_MEM = register_file_read_data_b_REG;

    always @(posedge clk or negedge rst_n) begin
        if (!rst_n) begin
            register_file_we_REG <= 0;
            data_we_REG <= 0;
            register_file_write_addr_REG <= 5'b0;
            register_file_write_data_is_from_mem_REG <= 0;
            register_file_write_data_is_from_ID_REG <= 0;
            register_file_write_data_ID_REG <= 32'b0;
            alu_res_REG <= 32'b0;
            register_file_read_data_b_REG <= 32'b0;
        end else begin
            register_file_we_REG <= register_file_we_EX;
            data_we_REG <= data_we_EX;
            register_file_write_addr_REG <= register_file_write_addr_EX;
            register_file_write_data_is_from_mem_REG <= register_file_write_data_is_from_mem_EX;
            register_file_write_data_is_from_ID_REG <= register_file_write_data_is_from_ID_EX;
            register_file_write_data_ID_REG <= register_file_write_data_ID_EX;
            alu_res_REG <= alu_res_EX;
            register_file_read_data_b_REG <= register_file_read_data_b_EX;
        end
    end

endmodule
