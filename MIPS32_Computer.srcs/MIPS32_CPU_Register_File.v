`timescale 1ns / 1ps

module MIPS32_CPU_Register_File(
    input clk,
    input rst_n,
    input [4:0] read_addr_a,
    input [4:0] read_addr_b,
    input we,
    input [4:0] write_addr,
    input [31:0] write_data,
    output [31:0] read_data_a,
    output [31:0] read_data_b
    );

    reg [31:0] registers [0:31];

    integer i;

    assign read_data_a = registers[read_addr_a];
    assign read_data_b = registers[read_addr_b];

    always @(posedge clk or negedge rst_n) begin
        if (!rst_n) begin
            for (i = 0;i < 32;i = i + 1) begin
                registers[i] <= 32'b0;
            end
        end else if (we && write_addr) begin
            registers[write_addr] <= write_data;
        end
    end

endmodule
