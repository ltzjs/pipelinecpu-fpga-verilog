`timescale 1ns / 1ps

module MIPS32_CPU(
    input clk,
    input rst_n,
    input [31:0] inst_data,
    input [31:0] data_in,
    output inst_en,
    output [31:0] inst_addr,
    output data_we,
    output [31:0] data_addr,
    output [31:0] data_out
    );

    wire pause_PC; // from Hazard_Unit
    wire [31:0] pc_IF;
    wire [31:0] pc_p4_IF = pc_IF + 32'd4;
    wire pc_next_is_pc_p4; // from Branch_Jump_Control_Unit
    wire [31:0] pc_branch_jump_ID; // from Branch_Jump_Control_Unit
    wire [31:0] pc_next = pc_next_is_pc_p4 ? pc_p4_IF : pc_branch_jump_ID;

    MIPS32_CPU_PC PC (
        // input:
        .clk(clk),
        .rst_n(rst_n),
        .pause(pause_PC),
        .pc_next(pc_next),
        // output:
        .pc_IF(pc_IF)
    );

    assign inst_addr = pc_IF;

    wire [31:0] inst_ID = inst_data;
    wire pause_REG_IF_ID; // from Hazard_Unit
    wire [31:0] pc_p4_ID;

    assign inst_en = ~pause_REG_IF_ID;

    MIPS32_CPU_REG_IF_ID REG_IF_ID (
        // input:
        .clk(clk),
        .rst_n(rst_n),
        .pause(pause_REG_IF_ID),
        .pc_p4_IF(pc_p4_IF),
        // output:
        .pc_p4_ID(pc_p4_ID)
    );

    wire [5:0] inst_opcode_ID = inst_ID[31:26];
    wire [4:0] inst_rs_ID = inst_ID[25:21];
    wire [4:0] inst_rt_ID = inst_ID[20:16];
    wire [4:0] inst_rd_ID = inst_ID[15:11];
    wire [5:0] inst_function_ID = inst_ID[5:0];
    wire [15:0] inst_immediate_ID = inst_ID[15:0];
    wire [25:0] inst_instr_index_ID = inst_ID[25:0];

    wire [31:0] sign_extended_inst_immediate_ID = {{16{inst_immediate_ID[15]}}, inst_immediate_ID};

    wire [31:0] alu_optr_ID;
    wire register_file_we_Control_Unit_ID;
    wire data_we_ID;
    wire alu_opnd_a_is_imm_ID;
    wire alu_opnd_b_is_imm_ID;
    wire register_file_write_addr_is_rt_ID;
    wire register_file_write_data_is_from_mem_ID;

    MIPS32_CPU_Control_Unit Control_Unit (
        // input:
        .inst_opcode(inst_opcode_ID),
        .inst_function(inst_function_ID),
        // output:
        .alu_optr(alu_optr_ID),
        .register_file_we_Control_Unit(register_file_we_Control_Unit_ID),
        .data_we(data_we_ID),
        .alu_opnd_a_is_imm(alu_opnd_a_is_imm_ID),
        .alu_opnd_b_is_imm(alu_opnd_b_is_imm_ID),
        .register_file_write_addr_is_rt(register_file_write_addr_is_rt_ID),
        .register_file_write_data_is_from_mem(register_file_write_data_is_from_mem_ID)
    );

    wire [4:0] register_file_read_addr_a_ID = inst_rs_ID;
    wire [4:0] register_file_read_addr_b_ID = inst_rt_ID;

    wire register_file_we_WB;
    wire [4:0] register_file_write_addr_WB;
    wire register_file_write_data_is_from_mem_WB;
    wire register_file_write_data_is_from_ID_WB;
    wire [31:0] register_file_write_data_ID_WB;
    wire [31:0] alu_res_WB;
    wire [31:0] data_WB = data_in;

    wire [31:0] register_file_read_data_a_ID; // rs
    wire [31:0] register_file_read_data_b_ID; // rt

    MIPS32_CPU_Register_File Register_File (
        // input:
        .clk(clk),
        .rst_n(rst_n),
        .read_addr_a(register_file_read_addr_a_ID),
        .read_addr_b(register_file_read_addr_b_ID),
        .we(register_file_we_WB),
        .write_addr(register_file_write_addr_WB),
        .write_data(register_file_write_data_is_from_ID_WB ? register_file_write_data_ID_WB : register_file_write_data_is_from_mem_WB ? data_WB : alu_res_WB),
        // output:
        .read_data_a(register_file_read_data_a_ID),
        .read_data_b(register_file_read_data_b_ID)
    );

    // Hazard_Unit output:
    wire register_file_read_data_a_is_outdated;
    wire register_file_read_data_b_is_outdated;
    wire [31:0] register_file_read_data_a_forwarded;
    wire [31:0] register_file_read_data_b_forwarded;
    wire nop_ID_EX;

    wire [31:0] register_file_read_data_a_updated_ID = register_file_read_data_a_is_outdated ? register_file_read_data_a_forwarded : register_file_read_data_a_ID;
    wire [31:0] register_file_read_data_b_updated_ID = register_file_read_data_b_is_outdated ? register_file_read_data_b_forwarded : register_file_read_data_b_ID;

    wire register_file_we_Branch_Jump_Control_Unit_ID;
    wire register_file_write_addr_is_31_ID;
    wire register_file_write_data_is_from_ID_ID = register_file_we_Branch_Jump_Control_Unit_ID;
    wire [31:0] register_file_write_data_ID_ID;

    MIPS32_CPU_Branch_Jump_Control_Unit Branch_Jump_Control_Unit (
        // input:
        .inst_opcode(inst_opcode_ID),
        .inst_rt(inst_rt_ID),
        .inst_function(inst_function_ID),
        .pc_p4(pc_p4_ID),
        // input for branch:
        .register_file_read_data_a_updated(register_file_read_data_a_updated_ID),
        .register_file_read_data_b_updated(register_file_read_data_b_updated_ID),
        .sign_extended_inst_immediate(sign_extended_inst_immediate_ID),
        // input for jump:
        .inst_instr_index(inst_instr_index_ID),
        // output:
        .pc_next_is_pc_p4(pc_next_is_pc_p4),
        .pc_branch_jump(pc_branch_jump_ID),

        .register_file_we_Branch_Jump_Control_Unit(register_file_we_Branch_Jump_Control_Unit_ID),
        .register_file_write_addr_is_31(register_file_write_addr_is_31_ID),
        .register_file_write_data(register_file_write_data_ID_ID)
    );

    wire register_file_we_ID = register_file_we_Control_Unit_ID | register_file_we_Branch_Jump_Control_Unit_ID;
    wire [4:0] register_file_write_addr_ID = (register_file_write_addr_is_rt_ID ? inst_rt_ID : inst_rd_ID) | {5{register_file_write_addr_is_31_ID}};

    wire [31:0] sign_extended_inst_immediate_EX;

    wire [31:0] alu_optr_EX;
    wire register_file_we_EX;
    wire data_we_EX;
    wire alu_opnd_a_is_imm_EX;
    wire alu_opnd_b_is_imm_EX;
    wire [4:0] register_file_write_addr_EX;
    wire register_file_write_data_is_from_mem_EX;
    wire register_file_write_data_is_from_ID_EX;
    wire [31:0] register_file_write_data_ID_EX;

    wire [31:0] register_file_read_data_a_EX;
    wire [31:0] register_file_read_data_b_EX;

    MIPS32_CPU_REG_ID_EX REG_ID_EX (
        // input:
        .clk(clk),
        .rst_n(rst_n),

        .nop_ID_EX(nop_ID_EX),

        .sign_extended_inst_immediate_ID(sign_extended_inst_immediate_ID),

        .alu_optr_ID(alu_optr_ID),
        .register_file_we_ID(register_file_we_ID),
        .data_we_ID(data_we_ID),
        .alu_opnd_a_is_imm_ID(alu_opnd_a_is_imm_ID),
        .alu_opnd_b_is_imm_ID(alu_opnd_b_is_imm_ID),
        .register_file_write_addr_ID(register_file_write_addr_ID),
        .register_file_write_data_is_from_mem_ID(register_file_write_data_is_from_mem_ID),
        .register_file_write_data_is_from_ID_ID(register_file_write_data_is_from_ID_ID),
        .register_file_write_data_ID_ID(register_file_write_data_ID_ID),

        .register_file_read_data_a_ID(register_file_read_data_a_updated_ID),
        .register_file_read_data_b_ID(register_file_read_data_b_updated_ID),

        // output:
        .sign_extended_inst_immediate_EX(sign_extended_inst_immediate_EX),

        .alu_optr_EX(alu_optr_EX),
        .register_file_we_EX(register_file_we_EX),
        .data_we_EX(data_we_EX),
        .alu_opnd_a_is_imm_EX(alu_opnd_a_is_imm_EX),
        .alu_opnd_b_is_imm_EX(alu_opnd_b_is_imm_EX),
        .register_file_write_addr_EX(register_file_write_addr_EX),
        .register_file_write_data_is_from_mem_EX(register_file_write_data_is_from_mem_EX),
        .register_file_write_data_is_from_ID_EX(register_file_write_data_is_from_ID_EX),
        .register_file_write_data_ID_EX(register_file_write_data_ID_EX),

        .register_file_read_data_a_EX(register_file_read_data_a_EX),
        .register_file_read_data_b_EX(register_file_read_data_b_EX)
    );

    wire [31:0] alu_res_EX;

    MIPS32_CPU_ALU ALU (
        // input:
        .optr(alu_optr_EX),
        .opnd_a(alu_opnd_a_is_imm_EX ? sign_extended_inst_immediate_EX : register_file_read_data_a_EX),
        .opnd_b(alu_opnd_b_is_imm_EX ? sign_extended_inst_immediate_EX : register_file_read_data_b_EX),
        // output:
        .res(alu_res_EX)
    );

    wire register_file_we_MEM;
    wire data_we_MEM;
    wire [4:0] register_file_write_addr_MEM;
    wire register_file_write_data_is_from_mem_MEM;
    wire register_file_write_data_is_from_ID_MEM;
    wire [31:0] register_file_write_data_ID_MEM;
    wire [31:0] alu_res_MEM;
    wire [31:0] register_file_read_data_b_MEM;

    MIPS32_CPU_REG_EX_MEM REG_EX_MEM (
        // input:
        .clk(clk),
        .rst_n(rst_n),

        .register_file_we_EX(register_file_we_EX),
        .data_we_EX(data_we_EX),
        .register_file_write_addr_EX(register_file_write_addr_EX),
        .register_file_write_data_is_from_mem_EX(register_file_write_data_is_from_mem_EX),
        .register_file_write_data_is_from_ID_EX(register_file_write_data_is_from_ID_EX),
        .register_file_write_data_ID_EX(register_file_write_data_ID_EX),
        .alu_res_EX(alu_res_EX),
        .register_file_read_data_b_EX(register_file_read_data_b_EX),

        // output:
        .register_file_we_MEM(register_file_we_MEM),
        .data_we_MEM(data_we_MEM),
        .register_file_write_addr_MEM(register_file_write_addr_MEM),
        .register_file_write_data_is_from_mem_MEM(register_file_write_data_is_from_mem_MEM),
        .register_file_write_data_is_from_ID_MEM(register_file_write_data_is_from_ID_MEM),
        .register_file_write_data_ID_MEM(register_file_write_data_ID_MEM),
        .alu_res_MEM(alu_res_MEM),
        .register_file_read_data_b_MEM(register_file_read_data_b_MEM)
    );

    assign data_we = data_we_MEM;
    assign data_addr = alu_res_MEM; // LW SW
    assign data_out = register_file_read_data_b_MEM; // SW

    MIPS32_CPU_REG_MEM_WB REG_MEM_WB (
        // input:
        .clk(clk),
        .rst_n(rst_n),

        .register_file_we_MEM(register_file_we_MEM),
        .register_file_write_addr_MEM(register_file_write_addr_MEM),
        .register_file_write_data_is_from_mem_MEM(register_file_write_data_is_from_mem_MEM),
        .register_file_write_data_is_from_ID_MEM(register_file_write_data_is_from_ID_MEM),
        .register_file_write_data_ID_MEM(register_file_write_data_ID_MEM),
        .alu_res_MEM(alu_res_MEM),

        // output:
        .register_file_we_WB(register_file_we_WB),
        .register_file_write_addr_WB(register_file_write_addr_WB),
        .register_file_write_data_is_from_mem_WB(register_file_write_data_is_from_mem_WB),
        .register_file_write_data_is_from_ID_WB(register_file_write_data_is_from_ID_WB),
        .register_file_write_data_ID_WB(register_file_write_data_ID_WB),
        .alu_res_WB(alu_res_WB)
    );

    MIPS32_CPU_Hazard_Unit Hazard_Unit (
        // input:
        .register_file_read_addr_a_ID(register_file_read_addr_a_ID),
        .register_file_read_addr_b_ID(register_file_read_addr_b_ID),
        .register_file_we_EX(register_file_we_EX),
        .register_file_we_MEM(register_file_we_MEM),
        .register_file_we_WB(register_file_we_WB),
        .register_file_write_addr_EX(register_file_write_addr_EX),
        .register_file_write_addr_MEM(register_file_write_addr_MEM),
        .register_file_write_addr_WB(register_file_write_addr_WB),
        .register_file_write_data_is_from_mem_EX(register_file_write_data_is_from_mem_EX),
        .register_file_write_data_is_from_mem_MEM(register_file_write_data_is_from_mem_MEM),
        .register_file_write_data_is_from_mem_WB(register_file_write_data_is_from_mem_WB),
        .register_file_write_data_is_from_ID_EX(register_file_write_data_is_from_ID_EX),
        .register_file_write_data_is_from_ID_MEM(register_file_write_data_is_from_ID_MEM),
        .register_file_write_data_is_from_ID_WB(register_file_write_data_is_from_ID_WB),
        .register_file_write_data_ID_EX(register_file_write_data_ID_EX),
        .register_file_write_data_ID_MEM(register_file_write_data_ID_MEM),
        .register_file_write_data_ID_WB(register_file_write_data_ID_WB),
        .alu_res_EX(alu_res_EX),
        .alu_res_MEM(alu_res_MEM),
        .alu_res_WB(alu_res_WB),
        .data_WB(data_WB),

        // output:
        .register_file_read_data_a_is_outdated(register_file_read_data_a_is_outdated),
        .register_file_read_data_b_is_outdated(register_file_read_data_b_is_outdated),
        .register_file_read_data_a_forwarded(register_file_read_data_a_forwarded),
        .register_file_read_data_b_forwarded(register_file_read_data_b_forwarded),
        .pause_PC(pause_PC),
        .pause_REG_IF_ID(pause_REG_IF_ID),
        .nop_ID_EX(nop_ID_EX)
    );

endmodule
