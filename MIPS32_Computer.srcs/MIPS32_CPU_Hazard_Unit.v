`timescale 1ns / 1ps

module MIPS32_CPU_Hazard_Unit(
    input [4:0] register_file_read_addr_a_ID,
    input [4:0] register_file_read_addr_b_ID,
    input register_file_we_EX,
    input register_file_we_MEM,
    input register_file_we_WB,
    input [4:0] register_file_write_addr_EX,
    input [4:0] register_file_write_addr_MEM,
    input [4:0] register_file_write_addr_WB,
    input register_file_write_data_is_from_mem_EX,
    input register_file_write_data_is_from_mem_MEM,
    input register_file_write_data_is_from_mem_WB,
    input register_file_write_data_is_from_ID_EX,
    input register_file_write_data_is_from_ID_MEM,
    input register_file_write_data_is_from_ID_WB,
    input [31:0] register_file_write_data_ID_EX,
    input [31:0] register_file_write_data_ID_MEM,
    input [31:0] register_file_write_data_ID_WB,
    input [31:0] alu_res_EX,
    input [31:0] alu_res_MEM,
    input [31:0] alu_res_WB,
    input [31:0] data_WB,

    output register_file_read_data_a_is_outdated,
    output register_file_read_data_b_is_outdated,
    output [31:0] register_file_read_data_a_forwarded,
    output [31:0] register_file_read_data_b_forwarded,
    output pause_PC,
    output pause_REG_IF_ID,
    output nop_ID_EX
    );

    wire register_file_read_after_write_a_EX = register_file_we_EX && register_file_write_addr_EX == register_file_read_addr_a_ID;
    wire register_file_read_after_write_a_MEM = register_file_we_MEM && register_file_write_addr_MEM == register_file_read_addr_a_ID;
    wire register_file_read_after_write_a_WB = register_file_we_WB && register_file_write_addr_WB == register_file_read_addr_a_ID;

    assign register_file_read_data_a_is_outdated = register_file_read_after_write_a_EX || register_file_read_after_write_a_MEM || register_file_read_after_write_a_WB;

    assign register_file_read_data_a_forwarded = register_file_read_after_write_a_EX ? register_file_write_data_is_from_ID_EX ? register_file_write_data_ID_EX : alu_res_EX
                                               : register_file_read_after_write_a_MEM ? register_file_write_data_is_from_ID_MEM ? register_file_write_data_ID_MEM : alu_res_MEM
                                               : register_file_read_after_write_a_WB ? register_file_write_data_is_from_ID_WB ? register_file_write_data_ID_WB : register_file_write_data_is_from_mem_WB ? data_WB : alu_res_WB
                                               : 32'b0;

    wire register_file_read_after_write_b_EX = register_file_we_EX && register_file_write_addr_EX == register_file_read_addr_b_ID;
    wire register_file_read_after_write_b_MEM = register_file_we_MEM && register_file_write_addr_MEM == register_file_read_addr_b_ID;
    wire register_file_read_after_write_b_WB = register_file_we_WB && register_file_write_addr_WB == register_file_read_addr_b_ID;

    assign register_file_read_data_b_is_outdated = register_file_read_after_write_b_EX || register_file_read_after_write_b_MEM || register_file_read_after_write_b_WB;

    assign register_file_read_data_b_forwarded = register_file_read_after_write_b_EX ? register_file_write_data_is_from_ID_EX ? register_file_write_data_ID_EX : alu_res_EX
                                               : register_file_read_after_write_b_MEM ? register_file_write_data_is_from_ID_MEM ? register_file_write_data_ID_MEM : alu_res_MEM
                                               : register_file_read_after_write_b_WB ? register_file_write_data_is_from_ID_WB ? register_file_write_data_ID_WB : register_file_write_data_is_from_mem_WB ? data_WB : alu_res_WB
                                               : 32'b0;

    assign pause_PC = nop_ID_EX;
    assign pause_REG_IF_ID = nop_ID_EX;

    assign nop_ID_EX = (register_file_write_data_is_from_mem_EX && (register_file_read_after_write_a_EX || register_file_read_after_write_b_EX))
                    || (register_file_write_data_is_from_mem_MEM && (register_file_read_after_write_a_MEM || register_file_read_after_write_b_MEM));

endmodule
